import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("belajar Kolom dan Baris"),
        ),
        body: Container(
          color: Colors.blue,
          margin: EdgeInsets.all(20),
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Text 1"),
              Text("Text 2"),
              Text("Text 3"),
              Row(
                children: <Widget>[
                  Text("Text 1"),
                  Text("text 2"),
                  Text("Text 3"),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
